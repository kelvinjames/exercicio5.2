
import utfpr.ct.dainf.if62c.pratica.Equacao2Grau;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica52 {
    public static void main(String[] args) {
        try{
            Equacao2Grau teste1 = new Equacao2Grau(1,9,8);
            System.out.println(teste1.getRaiz1());
            System.out.println(teste1.getRaiz2());
        }catch(RuntimeException erro){
            System.out.println(erro.getLocalizedMessage());
        }
        try{
            Equacao2Grau teste2 = new Equacao2Grau(0,9,8);
            System.out.println(teste2.getRaiz1());
            System.out.println(teste2.getRaiz2());
        }catch(RuntimeException erro){
            System.out.println(erro.getLocalizedMessage());
        }
        try{
            Equacao2Grau teste3 = new Equacao2Grau(1,-2,4);
            System.out.println(teste3.getRaiz1());
            System.out.println(teste3.getRaiz2());
        }catch(RuntimeException erro){
            System.out.println(erro.getLocalizedMessage());
        }
        try{
            Equacao2Grau teste4 = new Equacao2Grau(9,-24,16);
            System.out.println(teste4.getRaiz1());
            System.out.println(teste4.getRaiz2());
        }catch(RuntimeException erro){
            System.out.println(erro.getLocalizedMessage());
        }
    }
}
