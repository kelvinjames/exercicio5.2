/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;


public class Equacao2Grau<T extends Number> {
    T a;
    T b;
    T c;
    public Equacao2Grau(){
    }
    public Equacao2Grau(T a,T b,T c){
        if(a.doubleValue() == 0){
            throw new RuntimeException("Coeficiente a não pode ser zero");
        }
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public T getA() {
        return a;
    }

    public void setA(T a) {
        if(a.doubleValue() == 0){
            throw new RuntimeException("Coeficiente a não pode ser zero");
        }
        this.a = a;
    }
    public T getB() {
        return b;
    }

    public void setB(T b) {
        this.b = b;
    }

    public T getC() {
        return c;
    }

    public void setC(T c) {
        this.c = c;
    }
    public double getRaiz1(){
        double raiz1;
        if((Math.pow(this.b.doubleValue(),2) - (4 * this.a.doubleValue()) * this.c.doubleValue()) < 0){
            throw new RuntimeException("Equação não tem solução real");
        }
        raiz1 = (-this.b.doubleValue() - Math.sqrt(Math.pow(this.b.doubleValue(),2) - (4 * this.a.doubleValue()) * this.c.doubleValue()))/(2 * this.a.doubleValue());
        return raiz1;
    }   
    public double getRaiz2(){
        double raiz2;
        if((Math.pow(this.b.doubleValue(),2.0) - (4.0 * this.a.doubleValue()) * this.c.doubleValue()) < 0){
            throw new RuntimeException("Equação não tem solução real");
        }
        raiz2 = (- this.b.doubleValue() + Math.sqrt(Math.pow(this.b.doubleValue(),2.0) - (4.0 * this.a.doubleValue()) * this.c.doubleValue()))/(2 * this.a.doubleValue());
        return raiz2;
    }
}
